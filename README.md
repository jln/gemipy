# gemipy

Poste un toot lors de la publication d'une page gemini puis intègre le thread ainsi ouvert comme commentaires sur une page Gemini. Un exemple
est visible sur la page de test suivante : 

gemini://chezjln.xyz/des_commentaires_sur_gemini.gmi

## Installation

Ce script a besoin de quelques paquets pour fonctionner. Dans un environnement
virtuel (idéalement), exécuter :

```sh
pip install bs4 lxml python-frontmatter requests
```

## Usage

- Placer ses articles, écrits en gemtext, dans le dossier `source`. Ce dossier
  doit être créé au même endroit que ce script
- Insérer dans l'entête de l'article le paramètre suivant pour publier un toot

```
---
toot: True
---
```

- exécuter le script. Je bosse avec Python 3.9, je n'ai pas testé avec des
  versions inférieures
- uploader les fichiers produits dans le dossier `dest` sur son serveur Gemini

### L'entête

On vient de voir le premier paramètre renseignable dans l'entête : `toot`. Une
fois le toot publié, l'entête est mise à jour par le script pour intégrer l'ID
du toot, *via* le paramètre `toot-id`. Normalement, il ne faut pas y toucher. Sa
présence empêche une nouvelle publication du toot, et permet de récolter les
commentaires publier sur le Fediverse.

Si toutefois vous ne souhaitez pas ou plus insérer les commentaires dans votre
page, le paramètre optionnel `comments` peut être défini à `False`.

## Quelques remarques

- Quelques problèmes mineurs connus (cf. la gempage précédemment mentionnée)
- Vos articles peuvent être organisés dans différents sous-dossiers. *A priori*,
  le script reproduit bien l'arborescence
- Ce readme est écrit à l'arrache, je l'améliorerai ultérieurement ;-)

[jln](https://pleroma.chezjln.xyz/jln)
